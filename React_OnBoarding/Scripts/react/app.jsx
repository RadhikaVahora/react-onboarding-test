﻿import React from 'react';
import ReactDOM from 'react-dom';

//class Hello extends React.Component {
//    render() {
//        return <h1>Hello {this.props.message}!</h1>;
//    }
//}

ReactDOM.render(
    <h1>React Onboarding</h1>,
    document.getElementById('root')
)

