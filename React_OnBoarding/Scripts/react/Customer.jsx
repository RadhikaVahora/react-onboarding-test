﻿import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import { Button,Header, Footer, Modal } from 'semantic-ui-react';


export class AddCus extends React.Component {
   
    constructor() {
        super();
        this.state = {
            modalOpen: false
        };
    }
    handleOpen() {

        this.setState({ modalOpen: true });
    }
    handleClose() {

        this.setState({ modalOpen: false });
    }
    render() {
        return (
            <div>
                <Button onClick={this.handleOpen}>Show Modal</Button>

                <Modal open={this.state.modalOpen} onClose={this.handleClose}>
                    <Modal.Content>
                        <h3>This website uses cookies to ensure the best user experience.</h3>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={this.handleClose}>Got it</Button>
                    </Modal.Actions>
                </Modal>
            </div>
        )
    }
}



//export class Customer extends React.Component {
//    render() {
//        return (
//            <div>
//                <Button />
//            </div>
//            );
//    }
//}

ReactDOM.render(
    <AddCus/>,
    document.getElementById('customer')
)